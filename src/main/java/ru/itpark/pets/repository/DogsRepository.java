package ru.itpark.pets.repository;

import com.fasterxml.jackson.databind.util.Named;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.itpark.pets.domain.Dog;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository
public class DogsRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public DogsRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Dog> findAll() {
        return jdbcTemplate.query(
                "SELECT id, name, price, picture, content FROM dogs",
                (resultSet, i) -> new Dog(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("price"),
                        resultSet.getString("picture"),
                        resultSet.getString("content")
                )
        );
    }

    public Dog findById(int id) {
        return jdbcTemplate.queryForObject(
                "SELECT id, name, price, picture, content \n" +
                        "FROM items \n" +
                        "WHERE id = :id;",
                Map.of("id", id),
                (resultSet, i) -> new Dog (
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("price"),
                        resultSet.getString("picture"),
                        resultSet.getString("content")
                )
        );
    }


    public List<Dog> findByName(String name) {
        return jdbcTemplate.query(
                "SELECT id, name, price, picture, content \n" +
                        "FROM dogs\n" +
                        "WHERE name ILIKE :name;",
                Map.of("name", "%" + name + "%"),
                (resultSet, i) -> new Dog(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("price"),
                        resultSet.getString("picture"),
                        resultSet.getString("content")
                )
        );
    }

    public void removeById(int id) {
        jdbcTemplate.update(
                "DELETE FROM items WHERE id = :id",
                Map.of("id", id)
        );
    }

    public void add(Dog dog) {
        jdbcTemplate.update(
                "INSERT INTO dogs(name, price, picture, content) VALUES{:name, :price, :picture, :content}",
                Map.of("name",dog.getName(), "price", dog.getPrice(), "picture", dog.getPictureUrl(), "content", dog.getContent())
        );
    }

    public void save(Dog dog) {
        jdbcTemplate.update(
                "INSERT INTO dogs(picture) VALUES (:picture)",
                Map.of("picture", dog.getPictureUrl())
        );
    }
}